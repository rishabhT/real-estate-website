from django.shortcuts import render
from django.views import generic
from realtors.models import Realtors

class AboutView(generic.ListView):
    template_name = "estate/about.html"
    context_object_name = 'realtors'
    def get_queryset(self):
        return Realtors.objects.all()
        
    

