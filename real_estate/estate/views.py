from django.shortcuts import render
from listings.models import Listing
from django.views import generic
from listings.choices import state_choice,bedroom_value,price_choice

# def index(request):
#     listings = Listing.order_by('-list_date').filter(is_published=True)[:3]
#     context = {
#         'listings':listings
#     }
#     return render(request,"estate/index.html",context)
def about(request):
    return render(request,"estate/about.html")

class Index(generic.ListView):
    template_name = 'estate/index.html'
    context_object_name = 'listings'
    def get_queryset(self):
        return Listing.objects.order_by('-list_date').filter(is_published=True)[:3]

    def get_context_data(self, **kwargs):
        context = super(Index, self).get_context_data(**kwargs)
        context['state_choice'] = state_choice
        context['bedroom_value'] = bedroom_value
        context['price_choice'] = price_choice
        return context

    