from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic import CreateView
from accounts.forms import SignUpForm
from django.contrib.auth.models import User
# Sign Up View
class Register(CreateView):
    form_class = SignUpForm
    success_url = reverse_lazy('login')
    template_name = 'accounts/registration.html'
# # Create your views here.
# def register(request):
#     return render(request,"accounts/registration.html")

def login(request):
    return render(request,"accounts/login.html")

# def logout(request):
#     return render(request,"accounts/registration.html")

def dashboard(request):
    return render(request,"accounts/dashbord.html")