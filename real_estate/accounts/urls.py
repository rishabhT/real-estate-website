from django.contrib import admin
from django.urls import path
from accounts import views
urlpatterns = [
    path('registration',views.Register.as_view(),name="register"),
    path('login',views.login,name='login'),
    #path('logout',views.logout,'logout'),
    path('dashboard',views.dashboard,name='dashboard')
]