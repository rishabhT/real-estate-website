from django.urls import path
from listings import views
urlpatterns = [
    path("<int:listing_id>",views.listing,name="listing"),
    path("",views.IndexView.as_view(),name="listings"),
    path("search",views.SearchView.as_view(),name="search"),
]