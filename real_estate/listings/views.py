from django.shortcuts import render,get_object_or_404
from .models import Listing
from django.views import generic
from listings.choices import bedroom_value,state_choice,price_choice

def listing(request,listing_id):
    listing = get_object_or_404(Listing,pk=listing_id)
    context = {
        'listing' : listing
    }
    return render(request,"listings/listing.html",context)

def search(request):
    return render(request,"listings/search.html")

class IndexView(generic.ListView):
    template_name = "listings/listings.html"
    paginate_by  = 6
    context_object_name = 'listings'
    def get_queryset(self):
        return Listing.objects.all()

class SearchView(generic.ListView):
    template_name = 'listings/search.html'
    context_object_name = 'searchlists'

    def get_queryset(self):
        return Listing.objects.order_by('-list_date').filter(is_published=True)

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data(**kwargs)
        context['state_choice'] = state_choice
        context['bedroom_value'] = bedroom_value
        context['price_choice'] = price_choice
        return context
    def get(self, request, *args, **kwargs):
        listings=Listing.objects.order_by('-list_date')
        #keywords
        if 'keywords' in request.GET:
            keywords = request.GET.get('keywords')
            if keywords:
                listings=listings.filter(description__icontains=keywords)           
       
        # city
        if 'city' in request.GET:
            city = request.GET.get('city')
            if city:
                listings=listings.filter(city__iexact=city)          
        # state
        if 'state' in request.GET:
            state = request.GET.get('state')
            if state:
                listings=listings.filter(state__iexact=state)    

        # state
        if 'bedrooms' in request.GET:
            bedrooms = request.GET.get('bedrooms')
            if bedrooms:
                listings=listings.filter(bedrooms__lte=bedrooms)   

        # price
        if 'price' in request.GET:
            price = request.GET.get('price')
            if price:
                listings=listings.filter(price__lte=price)  
        
        return render(request, self.template_name, {'searchlist': listings})        